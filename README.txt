DESCRIPTION
===========
Adds Google Analytics event tracking to links.

Menu items
----------
To add event tracking to menu items, you need to install and enable the
menu_attributes module[1].

Link field
----------
This module integrates with the link module[1] if enabled. It can then be
enabled and configured in the field instance settings.


DEPENDENCIES
============
This module depends on the Google Analytics[3] module.


CREDITS
=======
Authored by Bendik R. Brenne <bendik@konstant.no>.


LINKS
=====
[1] http://drupal.org/project/menu_attributes
[2] http://drupal.org/project/link
[3] http://drupal.org/proejct/google_analytics