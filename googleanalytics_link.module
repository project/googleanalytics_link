<?php

/**
 * @file
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function googleanalytics_link_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  if ($form['#field']['type'] !== 'link_field')
    return;
  $settings =& $form['instance']['settings'];
  $instance = $form['#instance'];

  $settings['googleanalytics'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google Analytics settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $settings['googleanalytics']['enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable google analytics event tracking'),
    '#description' => t('Add Google Analytics <a href="!url" target="_blank">event tracking</a> to the link.', array(
      '!url' => 'http://developers.google.com/analytics/devguides/collection/gajs/eventTrackerGuide',
    )),
    '#default_value' => isset($instance['settings']['googleanalytics']['enable'])
        ? $instance['settings']['googleanalytics']['enable'] : 0,
  );

  $settings['googleanalytics']['category'] = array(
    '#type' => 'fieldset',
    '#title' => t('Parameter: %parameter', array('%parameter' => t('Category'))),
    '#description' => t('The name you supply for the group of objects you want to track.'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#states' => array(
      'invisible' => array(
        'input[name="instance[settings][googleanalytics][enable]"]' => array('checked' => FALSE),
      ),
    ),
  );
  $settings['googleanalytics']['category']['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#description' => t('The value to use by default. This field support tokens.'),
    '#default_value' => isset($instance['settings']['googleanalytics']['category']['value'])
        ? $instance['settings']['googleanalytics']['category']['value'] : '',
    '#element_validate' => array('googleanalytics_link_instance_settings_validate'),
  );
  $settings['googleanalytics']['category']['override'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow override'),
    '#default_value' => isset($instance['settings']['googleanalytics']['category']['override'])
        ? $instance['settings']['googleanalytics']['category']['override'] : 0,
  );

  $settings['googleanalytics']['action'] = array(
    '#type' => 'fieldset',
    '#title' => t('Parameter: %parameter', array('%parameter' => t('Action'))),
    '#description' => t('A string that is uniquely paired with each category, and commonly used to define the type of user interaction for the web object.'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#states' => array(
      'invisible' => array(
        'input[name="instance[settings][googleanalytics][enable]"]' => array('checked' => FALSE),
      ),
    ),
  );
  $settings['googleanalytics']['action']['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#description' => t('The value to use by default. This field support tokens.'),
    '#default_value' => isset($instance['settings']['googleanalytics']['action']['value'])
        ? $instance['settings']['googleanalytics']['action']['value'] : '',
    '#element_validate' => array('googleanalytics_link_instance_settings_validate'),
  );
  $settings['googleanalytics']['action']['override'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow override'),
    '#default_value' => isset($instance['settings']['googleanalytics']['action']['override'])
        ? $instance['settings']['googleanalytics']['action']['override'] : 0,
  );

  $settings['googleanalytics']['label'] = array(
    '#type' => 'fieldset',
    '#title' => t('Parameter: %parameter', array('%parameter' => t('Label'))),
    '#description' => t('An optional string to provide additional dimensions to the event data.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#states' => array(
      'invisible' => array(
        'input[name="instance[settings][googleanalytics][enable]"]' => array('checked' => FALSE),
      ),
    ),
  );
  $settings['googleanalytics']['label']['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#description' => t('The value to use by default. This field support tokens.'),
    '#default_value' => isset($instance['settings']['googleanalytics']['label']['value'])
        ? $instance['settings']['googleanalytics']['label']['value'] : '',
  );
  $settings['googleanalytics']['label']['override'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow override'),
    '#default_value' => isset($instance['settings']['googleanalytics']['label']['override'])
        ? $instance['settings']['googleanalytics']['label']['override'] : 0,
  );

  $settings['googleanalytics']['value'] = array(
    '#type' => 'fieldset',
    '#title' => t('Parameter: %parameter', array('%parameter' => t('Value'))),
    '#description' => t('An optional integer that you can use to provide numerical data about the user event.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#states' => array(
      'invisible' => array(
        'input[name="instance[settings][googleanalytics][enable]"]' => array('checked' => FALSE),
      ),
    ),
  );
  $settings['googleanalytics']['value']['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#description' => t('The value to use by default. This field support tokens.'),
    '#default_value' => isset($instance['settings']['googleanalytics']['value']['value'])
        ? $instance['settings']['googleanalytics']['value']['value'] : '',
  );
  $settings['googleanalytics']['value']['override'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow override'),
    '#default_value' => isset($instance['settings']['googleanalytics']['value']['override'])
        ? $instance['settings']['googleanalytics']['value']['override'] : 0,
  );


  if (module_exists('token')) {
    $settings['tokens']['#weight'] = 30;
  }
}

/**
 * Form validate callback.
 */
function googleanalytics_link_instance_settings_validate($element, &$form_state, $form) {
  if (isset($form_state['values']['instance']['settings']['googleanalytics']['enable'])
    && $form_state['values']['instance']['settings']['googleanalytics']['enable']) {

    $values =& $form_state['values']['instance']['settings']['googleanalytics'];
    $group = reset(array_slice($element['#parents'], -2, 1));
    $group_title = $form['instance']['settings']['googleanalytics'][$group]['#title'];

    if (!$values[$group]['override'] && empty($element['#value'])) {
      form_error($element, t('!group: A value is required when «Allow override» is disabled.', array(
        '!group' => $group_title,
      )));
    }
  }
}

/**
 * Implements hook_element_info_alter().
 *
 * Adds the google analytics event tracking widget to link fields.
 */
function googleanalytics_link_element_info_alter(&$types) {
  // Append a process function for the field integration.
  if (isset($types['link_field'])) {
    $types['link_field']['#process'][] = 'googleanalytics_link_process_widget';
  }
}

/**
 * Process callback.
 */
function googleanalytics_link_process_widget($element) {
  if (!isset($element['#entity_type'])) {
    return $element;
  }

  $field = field_info_field($element['#field_name']);
  $instance = field_info_instance($element['#entity_type'], $element['#field_name'], $element['#bundle']);

  if (isset($instance['settings']['googleanalytics']['enable']) && $instance['settings']['googleanalytics']['enable']) {
    $settings =& $instance['settings']['googleanalytics'];
    $attributes = isset($element['#value']['attributes']) ? $element['#value']['attributes'] : $settings['attributes'];
    if ($settings['category']['override'] || $settings['action']['override'] || $settings['label']['override'] || $settings['value']['override']) {
      $element['googleanalytics'] = array(
        '#type' => 'fieldset',
        '#title' => t('Google Analytics event tracking'),
        '#parents' => array_merge($element['#parents'], array('googleanalytics')),
        '#array_parents' => array_merge($element['#array_parents'], array('googleanalytics')),
      );
      if (empty($attributes['googleanalytics']) && $element['#value']) {
        $attributes['googleanalytics']['enable'] = 1;
      }
      _googleanalytics_link_process_element($element['googleanalytics'], $settings, $attributes['googleanalytics']);
    }
  }

  return $element;
}

/**
 * Element validate callback.
 */
function googleanalytics_link_element_validate($element, &$form_state, $form) {
  // Menu link
  if ($form['#form_id'] == 'menu_edit_item') {
    $values =& $form_state['values']['options']['attributes']['googleanalytics'];
    if ($values['enable']) {
      foreach (array('category' => FALSE, 'action' => FALSE, 'label' => TRUE, 'value' => TRUE) as $property_name => $optional) {
        if (!$optional && empty($values[$property_name])) {
          form_set_error('options][attributes][googleanalytics]['.$property_name, t('!field_name field: %property_name property is required.', array(
            '!field_name' => $element['#title'],
            '%property_name' => t($property_name),
          )));
        }
      }
    }

  // Link field
  } else {
    $field_name = $element['#parents'][0];
    $langcode = $element['#parents'][1];
    $delta = $element['#parents'][2];
    $values =& $form_state['values'][$field_name][$langcode][$delta]['googleanalytics'];
    $field_title = $form[$field_name][$langcode]['#title'];

    if ($values['enable']) {
      $entity =& $form['#entity'];
      // For now, we just assume this is a node..
      // https://drupal.org/node/1042822
      $entity_type = 'node';
      list(,,$bundle) = entity_extract_ids($entity_type, $entity);
      $instance = field_info_instance('node', $field_name, $bundle);
      $instance_settings = $instance['settings']['googleanalytics'];

      // Require a value for all non-optional properties which doesn't have a default value.
      foreach (array('category' => FALSE, 'action' => FALSE, 'label' => TRUE, 'value' => TRUE) as $property_name => $optional) {
        if ($instance_settings[$property_name]['override']
          && empty($instance_settings[$property_name]['value'])
          && !$optional
          && empty($values[$property_name])) {

          form_set_error($field_name.']['.$langcode.']['.$delta.'][googleanalytics]['.$property_name, t('!field_name field: %property_name property is required.', array(
            '!field_name' => $field_title,
            '%property_name' => t($property_name),
          )));
        }
      }
    }
  }
}

/**
 * Implements hook_field_attach_presave().
 */
function googleanalytics_link_field_attach_presave($entity_type, $entity) {
  list(,,$bundle) = entity_extract_ids($entity_type, $entity);
  $field_instances = field_info_instances($entity_type, $bundle);

  $fields = array();
  foreach ($field_instances as $field_name => $field) {
    if ($field['widget']['type'] == 'link_field') {
      if (isset($field['settings']['googleanalytics']) && $field['settings']['googleanalytics']['enable']) {
        $fields[] = $field_name;
      }
    }
  }
  $langcode = $entity->path['language'];

  foreach ($fields as $field_name) {
    foreach ($entity->{$field_name}[$langcode] as $delta => $field) {
      $field['attributes'] = array_merge(unserialize($field['attributes']), array(
        'googleanalytics' => $field['googleanalytics'],
      ));
      $entity->{$field_name}[$langcode][$delta]['attributes'] = serialize($field['attributes']);
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function googleanalytics_link_preprocess_link_formatter_link_default(&$variables) {
  if (isset($variables['field']['settings']['googleanalytics']) && $variables['field']['settings']['googleanalytics']['enable']) {
    $attributes =& $variables['element']['attributes'];
    // Explicit disable
    if (isset($attributes['googleanalytics']) && !$attributes['googleanalytics']['enable']) {

    } else {
      $overrides = isset($attributes['googleanalytics']) ? $attributes['googleanalytics'] : array();
      $values = $variables['field']['settings']['googleanalytics'];
      $values['enable'] = array('value' => 1);
      foreach ($values as $key => $default) {
        $attributes['googleanalytics'][$key] = (isset($overrides[$key]) && !empty($overrides[$key])) ? $overrides[$key] : $default['value'];
      }
    }
    _googleanalytics_link_preprocess($variables['element']);
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function googleanalytics_link_preprocess_menu_link(&$variables) {
  $attributes =& $variables['element']['#localized_options']['attributes'];
  if (isset($attributes['googleanalytics'])) {
    _googleanalytics_link_preprocess($variables['element']['#localized_options']);
  }
}

/**
 * Implements hook_menu_attribute_info().
 */
function googleanalytics_link_menu_attribute_info() {
  // Add Google Analytics attribute.
  $info['googleanalytics'] = array(
    'label' => t('Google Analytics'),
    'form' => array(
      '#type' => 'fieldset',
      '#process' => array('_googleanalytics_link_process_element'),
    ),
  );

  return $info;
}

/**
 * Implements hook_preprocess_HOOK().
 */
function googleanalytics_link_preprocess_html(&$variables) {
  $links =& drupal_static('googleanalytics_links', array());
  if ($links) {
    // TODO: token replacement

    drupal_add_js(drupal_get_path('module', 'googleanalytics_link') . '/js/googleanalytics_link.js');
    drupal_add_js(array('googleanalytics_link' => array(
      'links' => $links,
    )), 'setting');
  }
}

/**
 * Element process callback. Shared for both link fields and menu items.
 *
 * @param $element
 * @param $instance_settings
 * @param $default_values
 */
function _googleanalytics_link_process_element(&$element, $instance_settings = NULL, $default_values = NULL) {
  $element_parents = $element['#parents'][0] . '[' . implode('][', array_slice($element['#parents'], 1)) . ']';

  //
  if (!isset($default_values) && isset($element['#default_value'])) {
    $default_values = $element['#default_value'];
  }

  $element['enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable event tracking for this link'),
    '#default_value' => isset($default_values['enable']) ? $default_values['enable'] : 0,
  );
  $element['category'] = array(
    '#type' => 'textfield',
    '#title' => t('Category'),
    '#description' => t('The name you supply for the group of objects you want to track.'),
    '#size' => 15,
    '#default_value' => isset($default_values['category']) ? $default_values['category'] : '',
    '#states' => array(
      'invisible' => array(
        'input[name="' . $element_parents . '[enable]"]' => array('checked' => FALSE),
      ),
    ),
  );
  $element['action'] = array(
    '#type' => 'textfield',
    '#title' => t('Action'),
    '#description' => t('A string that is uniquely paired with each category, and commonly used to define the type of user interaction for the web object.'),
    '#size' => 15,
    '#default_value' => isset($default_values['action']) ? $default_values['action'] : '',
    '#states' => array(
      'invisible' => array(
        'input[name="' . $element_parents . '[enable]"]' => array('checked' => FALSE),
      ),
    ),
  );
  $element['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('An optional string to provide additional dimensions to the event data.'),
    '#size' => 15,
    '#default_value' => isset($default_values['label']) ? $default_values['label'] : '',
    '#states' => array(
      'invisible' => array(
        'input[name="' . $element_parents . '[enable]"]' => array('checked' => FALSE),
      ),
    ),
  );
  $element['value'] = array(
    '#type' => 'textfield',
    '#title' => t('Value'),
    '#description' => t('An optional string to provide additional dimensions to the event data.'),
    '#size' => 15,
    '#default_value' => isset($default_values['value']) ? $default_values['value'] : '',
    '#states' => array(
      'invisible' => array(
        'input[name="' . $element_parents . '[enable]"]' => array('checked' => FALSE),
      ),
    ),
  );

  //
  if (!isset($instance_settings)) {
    foreach (array('category', 'action') as $field_name) {
      $element[$field_name]['#states']['required'][] = array(
        'input[name="' . $element_parents . '[enable]"]' => array('checked' => TRUE),
      );
    }
  } else {
    foreach (array('category', 'action', 'label', 'value') as $field_name) {
      if (!empty($instance_settings[$field_name]['value'])) {
        $element[$field_name]['#field_suffix'] = t('Default: %value', array(
          '%value' => $instance_settings[$field_name]['value'],
        ));
        $element[$field_name]['#attributes']['placeholder'] = $instance_settings[$field_name]['value'];
      }
      $element[$field_name]['#access'] = $instance_settings[$field_name]['override'];
    }
  }

  $element['#element_validate'][] = 'googleanalytics_link_element_validate';
}

/**
 * @param $element
 */
function _googleanalytics_link_preprocess(&$element) {
  $links =& drupal_static('googleanalytics_links', array());
  $properties =& $element['attributes']['googleanalytics'];
  if ($properties['enable'] && !empty($properties['category']) && !empty($properties['action'])) {
    unset($element['attributes']['googleanalytics']['enable']);
    $html_id = drupal_html_id('ga-et');
    $element['attributes']['id'] = $html_id;
    $links[$html_id] = $element['attributes']['googleanalytics'];
  }
  unset($element['attributes']['googleanalytics']);
}