(function ($) {

  /**
   * Core behavior for Google Analytics Link.
   *
   * Binds to the click event on all configured links.
   */
  Drupal.behaviors.googleanalytics_link = {
    attach: function (context, settings) {
      var links = settings.googleanalytics_link.links,
          ids = $.map(links, function(value, id) {return '#'+id}).join(',');

      $(ids).once('ga-et', function() {
        $(this).click(function(event) {
          var config = links[this.id];
          Drupal.googleanalytics_link.track_event(config['category'], config['action'], config['label'], config['value']);
        });
      });
    }
  };

  /**
   * Internal methods.
   */
  Drupal.googleanalytics_link = {
    /**
     * Sends an event to analytics. Supports both ga.js and the new analytics.js.
     */
    track_event: function (category, action, label, value) {
      if (typeof(_gaq) !== 'undefined') {
        _gaq.push(['_trackEvent', category, action, label, value]);
      } else if (typeof(ga) !== 'undefined') {
        ga('send', 'event', category, action, label, value);
      }
    }
  };
})(jQuery);
